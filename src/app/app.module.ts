import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faTimes,
  faShoppingBag,
  faSearch,
} from '@fortawesome/free-solid-svg-icons';
import { FormsModule } from '@angular/forms';
import { library } from '@fortawesome/fontawesome-svg-core';
import { ProductsComponent } from './products/products.component';
import { ProductsHeaderComponent } from './products/products-header/products-header.component';
import { ProductsListComponent } from './products/products-list/products-list.component';
import { SearchInputComponent } from './shared/search-input/search-input.component';
import { ProductCardComponent } from './shared/product-card/product-card.component';
import { RatingModule } from 'ngx-bootstrap/rating';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductsHeaderComponent,
    ProductsListComponent,
    SearchInputComponent,
    ProductCardComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    RatingModule,
    PaginationModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  providers: [{ provide: Window, useValue: window }],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor() {
    library.add(faTimes, faShoppingBag, faSearch);
  }
}
