import {
  Component,
  OnInit,
  AfterContentInit,
  OnDestroy,
  Input,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { GetProductsResponse } from '../../models/get-products-response.model';
import { ProductsService } from '../../services/products.service';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { ResizeService } from '../../services/resize.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
})
export class ProductsListComponent
  implements OnInit, AfterContentInit, OnDestroy {
  @Input() public products: GetProductsResponse;
  @Input() public productsLoading: boolean;
  public currentPage: number;
  public pagesLimit: number = 4;
  public faShoppingBag = faShoppingBag;
  private pagesLimitSubscription: Subscription;
  private pageResetSubscription: Subscription;

  constructor(
    private productsService: ProductsService,
    private resizeService: ResizeService
  ) {}

  ngOnInit(): void {
    this.pageResetSubscription = this.productsService.pageReset.subscribe(
      () => {
        this.currentPage = 1;
      }
    );
    this.pagesLimitSubscription = this.resizeService.pagesLimitChanged.subscribe(
      (res: number) => {
        this.pagesLimit = res;
      }
    );
  }

  ngAfterContentInit(): void {
    this.resizeService.updatedPagesLimit();
  }

  ngOnDestroy(): void {
    this.pageResetSubscription.unsubscribe();
    this.pagesLimitSubscription.unsubscribe();
  }

  onPageChange(event: { page: number; itemsPerPage: number }) {
    this.productsService.pageChanged.next({ page: event.page });
  }
}
