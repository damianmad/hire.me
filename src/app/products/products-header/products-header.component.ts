import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-products-header',
  templateUrl: './products-header.component.html',
})
export class ProductsHeaderComponent implements OnInit {
  @Output() public searchInputsUpdated: EventEmitter<any> = new EventEmitter();
  private searchString: string = '';
  activeCheck: boolean = false;
  promoCheck: boolean = false;

  constructor(private productsService: ProductsService) {}

  ngOnInit(): void {}

  updateProducts() {
    this.productsService.headerInputsUpdated.next({
      search: this.searchString,
      promo: this.promoCheck,
      active: this.activeCheck,
    });
  }

  updateInputValue(searchString: string) {
    this.searchString = searchString;
    this.updateProducts();
  }
}
