import { Component, OnInit, OnDestroy, AfterContentInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from '../services/api.service';
import { ProductsService } from '../services/products.service';
import { ResizeService } from '../services/resize.service';
import { SearchParams } from '../models/search-params.model';
import { GetProductsResponse } from '../models/get-products-response.model';
import { HeaderInputs } from '../models/header-inputs.model';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
})
export class ProductsComponent implements OnInit, AfterContentInit, OnDestroy {
  public products: GetProductsResponse;
  public productsLoading: boolean = false;
  private searchParams: SearchParams = {
    search: '',
    limit: 4,
    page: 1,
  };
  private headerInputsSubscription: Subscription;
  private pageChangedSubscription: Subscription;
  private widthBreakpointSubscription: Subscription;

  constructor(
    private api: ApiService,
    private productsService: ProductsService,
    private resizeService: ResizeService
  ) {}

  ngOnInit(): void {
    this.headerInputsSubscription = this.productsService.headerInputsUpdated.subscribe(
      (res: HeaderInputs) => {
        this.productsService.pageReset.next();
        this.updateSearchParams({ ...res, page: 1 });
      }
    );
    this.pageChangedSubscription = this.productsService.pageChanged.subscribe(
      (res: { page: number }) => {
        this.updateSearchParams(res);
      }
    );
    this.resizeService.setupResizeDetection();
    this.widthBreakpointSubscription = this.resizeService.productsLimitChanged.subscribe(
      (res: number) => {
        if (!this.products || this.products.meta.totalPages > 1) {
          this.productsService.pageReset.next();
          this.updateSearchParams({ limit: res, page: 1 });
        } else {
          this.searchParams.limit = res;
        }
      }
    );
  }

  ngAfterContentInit(): void {
    this.resizeService.updateProductsLimit();
  }

  ngOnDestroy(): void {
    this.headerInputsSubscription.unsubscribe();
    this.pageChangedSubscription.unsubscribe();
    this.widthBreakpointSubscription.unsubscribe();
  }

  updateSearchParams(updatedAttributes: Object) {
    this.productsLoading = true;
    this.searchParams = Object.assign(this.searchParams, updatedAttributes);
    this.api.getProducts(this.searchParams).subscribe(
      (res: GetProductsResponse) => {
        this.products = res;
        this.productsLoading = false;
      },
      (error: HttpErrorResponse) => {
        console.error(error);
      }
    );
  }
}
