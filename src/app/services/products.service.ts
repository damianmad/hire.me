import { Injectable } from '@angular/core';
import { HeaderInputs } from '../models/header-inputs.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  headerInputsUpdated: Subject<HeaderInputs> = new Subject();
  pageChanged: Subject<{ page: number }> = new Subject();
  pageReset: Subject<true> = new Subject();
  constructor() {}
}
