import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { SearchParams } from '../models/search-params.model';
import { GetProductsResponse } from '../models/get-products-response.model';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  stringifyPropertyValues(params) {
    const stringifiedObject = {};
    for (const key in params) {
      stringifiedObject[key] = String(params[key]);
    }
    return stringifiedObject;
  }

  getProducts(params: SearchParams) {
    if (params.promo === false) {
      delete params.promo;
    }
    if (params.active === false) {
      delete params.active;
    }
    const stringifiedParams = this.stringifyPropertyValues(params);
    const requestParams = new HttpParams({ fromObject: stringifiedParams });

    return this.http.get<GetProductsResponse>(
      'https://join-tsh-api-staging.herokuapp.com/product',
      {
        headers: new HttpHeaders({ accept: 'application/json' }),
        params: requestParams,
      }
    );
  }
}
