import { Injectable } from '@angular/core';
import { fromEvent, Observable, Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Breakpoints } from '../models/breakpoints.enum';
import { ProductsLimits, PagesLimits } from '../models/limits.enum';

@Injectable({
  providedIn: 'root',
})
export class ResizeService {
  public resizeSubscription: Subscription;
  public resizeObservable: Observable<Event>;
  public productsLimitChanged: Subject<number> = new Subject();
  public pagesLimitChanged: Subject<number> = new Subject();
  private productsLimit: number;
  private pagesLimit: number;

  constructor(private window: Window) {}

  setupResizeDetection() {
    this.resizeObservable = fromEvent(window, 'resize');
    this.resizeSubscription = this.resizeObservable
      .pipe(debounceTime(1000))
      .subscribe(() => {
        this.updateProductsLimit();
        this.updatedPagesLimit();
      });
  }

  updateProductsLimit() {
    let updatedProductsLimit: number;
    if (this.window.innerWidth <= Breakpoints.Large) {
      updatedProductsLimit = ProductsLimits.Small;
    } else if (
      this.window.innerWidth >= Breakpoints.Large &&
      this.window.innerWidth <= Breakpoints.XL
    ) {
      updatedProductsLimit = ProductsLimits.Large;
    } else if (this.window.innerWidth >= Breakpoints.XL) {
      updatedProductsLimit = ProductsLimits.XL;
    }
    if (updatedProductsLimit !== this.productsLimit) {
      this.productsLimitChanged.next(updatedProductsLimit);
      this.productsLimit = updatedProductsLimit;
    }
  }

  updatedPagesLimit() {
    let updatedPagesLimit: number;
    if (this.window.innerWidth <= Breakpoints.Large) {
      updatedPagesLimit = PagesLimits.Small;
    } else if (
      this.window.innerWidth >= Breakpoints.Large &&
      this.window.innerWidth <= Breakpoints.XL
    ) {
      updatedPagesLimit = PagesLimits.Large;
    } else if (this.window.innerWidth >= Breakpoints.XL) {
      updatedPagesLimit = PagesLimits.XL;
    }
    if (updatedPagesLimit !== this.pagesLimit) {
      this.pagesLimitChanged.next(updatedPagesLimit);
      this.pagesLimit = updatedPagesLimit;
    }
  }
}
