export enum ProductsLimits {
  Small = 4,
  Large = 6,
  XL = 8,
}

export enum PagesLimits {
  Small = 4,
  Large = 6,
  XL = 10,
}
