import { HeaderInputs } from './header-inputs.model';

export interface SearchParams extends HeaderInputs {
  limit: number;
  page: number;
}
