export enum Breakpoints {
  Large = 992,
  XL = 1200,
}
