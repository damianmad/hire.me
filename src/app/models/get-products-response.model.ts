import { Product } from './product.model';

export interface GetProductsResponse {
  items: Product[];
  links: {
    first: string;
    last: string;
    next: string;
    previour: string;
  };
  meta: {
    currentPage: string;
    itemCount: number;
    itemsPerPage: string;
    totalItems: number;
    totalPages: number;
  };
}
