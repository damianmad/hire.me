export interface HeaderInputs {
  search: string;
  promo?: boolean;
  active?: boolean;
}
