import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { Product } from '../../models/product.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
})
export class ProductCardComponent implements OnInit {
  @Input() public product: Product;
  public bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) {}

  ngOnInit(): void {}

  openProductModal(template: TemplateRef<any>) {
    let modalClasses = 'product-modal modal-dialog-centered';
    const modalOptions = {
      initialState: { product: this.product },
      class: modalClasses,
    };
    this.bsModalRef = this.modalService.show(template, modalOptions);
  }
}
