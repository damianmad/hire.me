import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faSearch, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
})
export class SearchInputComponent implements OnInit {
  public searchString: string;
  @Output() public valueChanged: EventEmitter<string> = new EventEmitter();
  public faSearch: IconDefinition = faSearch;
  constructor() {}

  ngOnInit(): void {}

  onChange() {
    this.valueChanged.emit(this.searchString);
  }
}
